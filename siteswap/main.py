from siteswap.engine.state_graph import StateGraph
from siteswap.engine.utils.siteswap_utils import print_statistics, filter_siteswaps
import sys


if __name__ == "__main__":
    n_objects = 4
    max_throw_height = "7"
    sys.setrecursionlimit(10000)
    g = StateGraph.create(n_objects, max_throw_height)
    g.print_stats()
    siteswaps, siteswaps_e = g.get_prime_patterns()
    print_statistics(siteswaps, f"# All prime ground state patterns for {n_objects} objects "
                                f"and max throw height {max_throw_height}")
    # print_statistics(siteswaps_e, f"# All prime excited state patterns for {n_objects} objects "
    #                               f"and max throw height {max_throw_height}")
    siteswaps = filter_siteswaps(siteswaps,
                                 min_length=7,
                                 exclude_sequences=["0", "11", "2"],
                                 include_sequences=["7", "641"],
                                 max_connected_same_height_throws=1,
                                 max_same_height_throws=2,
                                 max_repetition_length=1,
                                 min_repetitions_for_exclusion=2)
    print_statistics(siteswaps, f"# Filtered prime ground state patterns for {n_objects} objects "
                                f"and max throw height {max_throw_height}")
