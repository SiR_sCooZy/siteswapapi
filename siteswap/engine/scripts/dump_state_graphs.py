import sys
from queue import Queue
from typing import List

from siteswap.engine.scripts.time_boxed_task import run_task
from siteswap.engine.state_graph import StateGraph
from siteswap.engine.utils.siteswap_utils import encode_throw_height
import os


def dump_state_graph_task(queue: Queue, n_objects: int, throw_height: str) -> None:
    try:
        # DFS on these graphs can get pretty nasty...
        sys.setrecursionlimit(10000)
        g = StateGraph.create(n_objects, throw_height)
        dump_path = fr"{os.getcwd()}\data\graphs\state_graph_{n_objects}n_{throw_height}.json"
        g.save_as_json(dump_path)
        queue.put((True, None))
    except Exception as e:
        queue.put((False, str(e)))


if __name__ == '__main__':
    failed_tasks = []
    for num_objects in range(1, 13):
        for max_height in range(num_objects, num_objects + 10):
            max_h = encode_throw_height(max_height)
            print(f"Starting creation for state graph {num_objects} objects, max height {max_h}")
            completed = run_task(dump_state_graph_task, 60, num_objects, max_h)
            if completed:
                print("Success")
            else:
                print(f"# Terminating for {num_objects} objects")
                failed_tasks.append((num_objects, max_h))
                break
    print("__________________")
    print("Failed Tasks")
    for num_objects, max_h in failed_tasks:
        print(f"{num_objects} objects - max height {max_h}")
    print("__________________")
