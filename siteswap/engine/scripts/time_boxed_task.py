import multiprocessing
import queue
from typing import Callable


def run_task(task: Callable[..., None], timeout: int, *args) -> bool:
    q = multiprocessing.Queue()
    p = multiprocessing.Process(target=task, args=(q, *args))
    p.start()
    try:
        ret_value = q.get(timeout=timeout)
    except queue.Empty:
        if p.is_alive():
            p.terminate()
            p.join()
        print(f"Failed task due to timeout after {timeout} seconds")
        return False
    else:
        if ret_value[0]:
            return True
        else:
            print(f"Failed task with error message: {ret_value[1]}")
            return False
