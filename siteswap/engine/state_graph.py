from __future__ import annotations

import json
from pathlib import Path
from typing import List, Dict, Tuple, Optional

import networkx as nx
from siteswap.engine.utils.siteswap_utils import sort_siteswaps, encode_throw_height, decode_throw_height


class StateGraph:
    Edge = Tuple[str, str]
    Edges = Dict[Edge, str]

    def __init__(self, nodes: Optional[List[str]] = None, edges: Optional[Edges] = None,
                 n: Optional[int] = None, h: Optional[str] = None):
        if n is not None and h is not None:
            h_dec = decode_throw_height(h)
            if h_dec < n:
                raise ValueError("Height must be at least as large as the number of objects")
            self.base_state = "1" * n + "0" * (h_dec - n)
            self.nodes = [self.base_state]
            self.edges = {}
            self._create_state_graph_rec(self.base_state)
        elif nodes is not None and edges is not None:
            n = sum([1 for t in nodes[0] if t == "1"])
            h_dec = len(nodes[0])
            self.base_state = "1" * n + "0" * (h_dec - n)
            self.nodes = nodes
            self.edges = edges
        else:
            raise ValueError("Either both 'nodes' and 'edges' or both 'n' and 'h' must be specified")
        self.graph = nx.DiGraph()
        self.graph.add_nodes_from(self.nodes)
        self.graph.add_edges_from(self.edges.keys())

    @classmethod
    def create(cls, n: int, h: str) -> StateGraph:
        return cls(n=n, h=h)

    @classmethod
    def create_from_nodes_and_edges(cls, nodes: List[str], edges: Edges) -> StateGraph:
        return cls(nodes=nodes, edges=edges)

    @classmethod
    def create_from_json(cls, path: str) -> StateGraph:
        with open(path, 'r') as f:
            graph_data = json.load(f)
        nodes = graph_data["nodes"]
        edges = {(*k.split(","), ): v for k, v in graph_data["edges"].items()}
        return StateGraph.create_from_nodes_and_edges(nodes, edges)

    def save_as_json(self, out_path: str) -> None:
        Path(out_path).parent.mkdir(exist_ok=True, parents=True)
        export_dict = self._as_dict()
        with open(out_path, 'w') as f:
            json.dump(export_dict, f)

    def _create_state_graph_rec(self, state: str) -> None:
        tmp_state = state[1:] + "0"
        can_throw = state[0] == "1"
        if can_throw:
            for i, targeted in enumerate(tmp_state):
                if targeted == "0":
                    next_state = tmp_state[:i] + "1" + tmp_state[i + 1:]
                    self._create_state_graph_rec_expand(state, next_state, encode_throw_height(i + 1))
        else:
            self._create_state_graph_rec_expand(state, tmp_state, "0")

    def _create_state_graph_rec_expand(self, cur_state: str, next_state: str, transition_throw: str) -> None:
        edge = (cur_state, next_state)
        self.edges[edge] = transition_throw
        if next_state not in self.nodes:
            self.nodes.append(next_state)
            self._create_state_graph_rec(next_state)

    def _decode_siteswap_from_states(self, states: List[str]) -> str:
        siteswap = ""
        rotated_states = states[1:] + states[:1]
        for state, next_state in zip(states, rotated_states):
            edge = (state, next_state)
            siteswap += self.edges[edge]
        # rotate siteswap such that it starts from base state (if possible)
        # won't do anything for excited state patterns
        for i, state in enumerate(states):
            if state == self.base_state:
                siteswap = siteswap[i:] + siteswap[:i]
                break
        return siteswap

    def print_stats(self) -> None:
        print("_____________________________")
        print("Graph Statistics")
        print("_____________________________")
        print(f"Node Count {len(self.nodes)}")
        print(f"Edge Count {len(self.edges)}")
        print("_____________________________")

    def get_prime_patterns(self, max_length: Optional[int] = None) -> Tuple[List[str], List[str]]:
        if max_length:
            raise NotImplementedError("Not yet...")
        else:
            search_graph = self.graph
        # use Johnson’s algorithm to find all simple cycles
        patterns_states = list(nx.simple_cycles(search_graph))
        base_siteswaps = []
        excited_siteswaps = []
        for pattern_states in patterns_states:
            siteswap = self._decode_siteswap_from_states(pattern_states)
            is_base_pattern = self.base_state in pattern_states
            if is_base_pattern:
                base_siteswaps.append(siteswap)
            else:
                excited_siteswaps.append(siteswap)
        sort_siteswaps(base_siteswaps)
        sort_siteswaps(excited_siteswaps)
        return base_siteswaps, excited_siteswaps

    def _as_dict(self) -> Dict[str, any]:
        return {
            "nodes": self.nodes,
            "edges": {",".join(e): th for e, th in self.edges.items()}
        }
