from collections import Counter
from typing import List, Optional, TypeVar, Callable, Iterable

from siteswap.engine.utils.repetition_utils import repetition_has_longer_length, max_connected_same_chars


ENCODE_THROW = {
    0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: 'a', 11: 'b', 12: 'c',
    13: 'd', 14: 'e', 15: 'f', 16: 'g', 17: 'h', 18: 'i', 19: 'j', 20: 'k', 21: 'l', 22: 'm', 23: 'n', 24: 'o',
    25: 'p', 26: 'q', 27: 'r', 28: 's', 29: 't', 30: 'u', 31: 'v', 32: 'w', 33: 'x', 34: 'y', 35: 'z'
}

DECODE_THROW = {
    '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, 'a': 10, 'b': 11, 'c': 12,
    'd': 13, 'e': 14, 'f': 15, 'g': 16, 'h': 17, 'i': 18, 'j': 19, 'k': 20, 'l': 21, 'm': 22, 'n': 23, 'o': 24,
    'p': 25, 'q': 26, 'r': 27, 's': 28, 't': 29, 'u': 30, 'v': 31, 'w': 32, 'x': 33, 'y': 34, 'z': 35
}


def encode_throw_height(throw_height: int) -> str:
    return ENCODE_THROW[throw_height]


def decode_throw_height(throw_height: str) -> int:
    return DECODE_THROW[throw_height]


def sort_siteswaps(siteswaps: List[str], by_length: bool = True) -> None:
    if by_length:
        siteswaps.sort(key=lambda siteswap: (len(siteswap), siteswap))
    else:
        siteswaps.sort()


T = TypeVar('T')


def _filter(it: Iterable[T], filter_func: Callable[[T], bool]) -> List[T]:
    return [e for e in it if filter_func(e)]


def filter_siteswaps(siteswaps: List[str],
                     min_length: Optional[int] = None,
                     max_length: Optional[int] = None,
                     max_height: Optional[int] = None,
                     max_same_height_throws: Optional[int] = None,
                     max_connected_same_height_throws: Optional[int] = None,
                     exclude_sequences: Optional[List[str]] = None,
                     include_sequences: Optional[List[str]] = None,
                     max_repetition_length: Optional[int] = None,
                     min_repetitions_for_exclusion: int = 2) -> List[str]:
    # run fast checks first for more efficiency
    if min_length:
        siteswaps = _filter(siteswaps, lambda s: len(s) >= min_length)
    if max_length:
        siteswaps = _filter(siteswaps, lambda s: len(s) <= max_length)
    if max_height:
        siteswaps = _filter(siteswaps, lambda s: max([int(c) for c in s]) <= max_height)
    if exclude_sequences:
        siteswaps = _filter(siteswaps, lambda s: all([seq not in s for seq in exclude_sequences]))
    if include_sequences:
        siteswaps = _filter(siteswaps, lambda s: all([seq in s for seq in include_sequences]))
    if max_same_height_throws:
        siteswaps = _filter(siteswaps, lambda s: all([v <= max_same_height_throws for v in Counter(s).values()]))
    if max_connected_same_height_throws:
        siteswaps = _filter(siteswaps, lambda s: max_connected_same_chars(s) <= max_connected_same_height_throws)
    if max_repetition_length:
        siteswaps = _filter(siteswaps, lambda s: not repetition_has_longer_length(s, max_repetition_length,
                                                                                  min_repetitions_for_exclusion))
    return siteswaps


def print_statistics(siteswaps: List[str], annotation: Optional[str] = None) -> None:
    sort_siteswaps(siteswaps)
    print("_____________________________")
    print("List of Siteswaps")
    if annotation:
        print(annotation)
    print("_____________________________")
    for siteswap in siteswaps:
        print(siteswap)
    print("_____________________________")
    print("Statistics")
    print("_____________________________")
    pattern_lengths = [len(siteswap) for siteswap in siteswaps]
    for pattern_length, count in Counter(pattern_lengths).items():
        print(f"Pattern length {pattern_length}\t->\t{count} siteswaps")
    print("_____________________________")
    print(f"Total Siteswaps {len(siteswaps)}")
    print("_____________________________")
