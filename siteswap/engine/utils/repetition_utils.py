from collections import defaultdict
from typing import Optional, Tuple, Iterator


def _get_substrings(s: str, start_from: int) -> Iterator[str]:
    substr = s[start_from:]
    i = -1
    while substr:
        yield substr
        substr = s[start_from:i]
        i -= 1


def longest_substring_repetition(s: str, min_repetitions: int = 2) -> Optional[Tuple[str, int]]:
    substring_counts = defaultdict(int)
    for i in range(len(s)):
        for substring in _get_substrings(s, i):
            substring_counts[substring] += 1

    substrings_filtered = [k for k, v in substring_counts.items() if v >= min_repetitions]

    if substrings_filtered:
        longest_substring = max(substrings_filtered,
                                key=lambda sub: (len(sub), substring_counts[sub]))
        return longest_substring, substring_counts[longest_substring]
    else:
        return None


def repetition_has_longer_length(s: str, length: int, min_repetitions: int = 2) -> bool:
    if length < 1:
        raise ValueError("Reference length for finding repetitions must be at least 1.")
    if min_repetitions < 2:
        raise ValueError("There must be at least 2 repetitions.")
    substring_counts = defaultdict(int)
    for i in range(len(s)):
        for substring in _get_substrings(s, i):
            substring_counts[substring] += 1
    survivors = [s for s, c in substring_counts.items() if c >= min_repetitions and len(s) > length]
    return len(survivors) > 0


def max_connected_same_chars(string: str) -> int:
    max_repetitions = -1
    cur_repetitions = 1
    for i in range(len(string) - 1):
        cur_char = string[i]
        next_char = string[i + 1]
        if cur_char == next_char:
            cur_repetitions += 1
        else:
            if cur_repetitions > max_repetitions:
                max_repetitions = cur_repetitions
            cur_repetitions = 1
    if cur_repetitions > max_repetitions:
        max_repetitions = cur_repetitions
    return max_repetitions
