import unittest

from siteswap.engine.utils.siteswap_utils import filter_siteswaps


class TestStringMethods(unittest.TestCase):

    def test_siteswap_filters_individually(self):
        f = filter_siteswaps
        self.assertListEqual(f(["53"], min_length=2), ["53"])
        self.assertListEqual(f(["53"], min_length=3), [])
        self.assertListEqual(f(["53"], max_length=2), ["53"])
        self.assertListEqual(f(["53"], max_length=1), [])
        self.assertListEqual(f(["53"], max_height=5), ["53"])
        self.assertListEqual(f(["53"], max_height=4), [])
        self.assertListEqual(f(["423"], exclude_sequences=["0", "1", "34"]), ["423"])
        self.assertListEqual(f(["531"], exclude_sequences=["0", "1", "34"]), [])
        self.assertListEqual(f(["423", "531"], exclude_sequences=["23"]), ["531"])
        self.assertListEqual(f(["423", "531"], include_sequences=["23"]), ["423"])
        self.assertListEqual(f(["531", "441"], include_sequences=["5", "1"]), ["531"])
        self.assertListEqual(f(["5751717241"], max_same_height_throws=3), ["5751717241"])
        self.assertListEqual(f(["5751717241"], max_same_height_throws=2), [])
        self.assertListEqual(f(["7372277131"], max_connected_same_height_throws=1), [])
        self.assertListEqual(f(["7372277131"], max_connected_same_height_throws=2), ["7372277131"])
        self.assertListEqual(f(["673175151"], max_repetition_length=2), ["673175151"])
        self.assertListEqual(f(["673175151"], max_repetition_length=1), [])
        self.assertListEqual(f(["773171731641"], max_repetition_length=2), [])
        self.assertListEqual(f(["773171731641"], max_repetition_length=3), ["773171731641"])
        self.assertListEqual(f(["7722717223"], max_repetition_length=2, min_repetitions_for_exclusion=3),
                             ["7722717223"])
        self.assertListEqual(f(["7722717223"], max_repetition_length=2, min_repetitions_for_exclusion=2), [])
        self.assertListEqual(f(["77317173177131"], max_repetition_length=3, min_repetitions_for_exclusion=2), [])
        self.assertListEqual(f(["77317173177131"], max_repetition_length=4, min_repetitions_for_exclusion=2),
                             ["77317173177131"])
        self.assertListEqual(f(["77317173177131"], max_repetition_length=2, min_repetitions_for_exclusion=3),
                             ["77317173177131"])
        self.assertListEqual(f(["561551"], max_repetition_length=1, min_repetitions_for_exclusion=2), ["561551"])
